import React from 'react';
import Header from './components/Header';
import FilterSettings from './components/FilterSettings';
import Chart from './components/Chart';

import _ from 'lodash';

import './App.css';

const loadData = async () => {
  try {
    let response = await fetch('http://adverity-challenge.s3-website-eu-west-1.amazonaws.com/DAMKBAoDBwoDBAkOBAYFCw.csv');
    let csvData =
      (await response.text())
        .split('\n')
        .map(entry => entry.split(','))
        .filter(entry => !!entry.join(''));

    let data = csvData.slice(1).map(columns => {
      const o = {};
      columns.forEach((column, index) => {
        o[_.head(csvData)[index]] = column;
      });
      return o;
    });

    return data;

  } catch (err) {
    console.error(err.message);
  }
}

class App extends React.Component {

  state = {
    data: null,
    filteredData: null
  }

  constructor() {
    super();
    loadData()
      .then(data => this.setState({ data }))
      .catch(err => console.error(err.message));
  }

  applyFilterHandler = (data, filters) => this.setState({ filteredData: data, filters });

  render() {    
    let { data, filteredData, filters } = this.state;

    return (
      <React.Fragment>
        <h1>Adverity Advertising Data ETL-V Challenge<span style={{ display: 'block', fontSize: '14px' }}>by Arda Cansiz</span></h1>
        <Header />
        <FilterSettings data={data} applyFilterHandler={this.applyFilterHandler}/>
        <Chart data={data} filteredData={filteredData} filters={filters} />
      </React.Fragment>
    );
  }
}

export default App;
