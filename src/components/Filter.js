import React, { Component } from 'react';
import { ReactComponent as Logo } from '../images/icons/close-24px.svg'

class Filter extends Component {
  state = {
    selectedValues: []
  }

  constructor() {
    super();
    this.inputElement = React.createRef();
  }

  addValue = value => this.state.selectedValues.includes(value) ? null : this.setState({ selectedValues: [...this.state.selectedValues, value]});
  
  removeValue = value => this.state.selectedValues.includes(value) ? this.setState({ selectedValues: this.state.selectedValues.filter(v => v !== value)}) : null;

  reset = inputElement => this.inputElement.current.value = this.inputElement.current.defaultValue;
  
  resetFilter = () => this.state.selectedValues?.length > 0 ? this.setState({ selectedValues: [] }) : null;

  componentDidUpdate = () => {
    this.props.handleSelectedValues(this.state.selectedValues);
    if (this.state.selectedValues?.length < 1) this.reset();
  }

  render() {
    return (
      <label htmlFor={this.props.name}>{this.state.selectedValues.length > 0 ? <button onClick={(ev => { ev.preventDefault(); this.resetFilter(); })}><Logo />{this.props.name}</button> : this.props.name}
        <select id={this.props.name} ref={this.inputElement} onChange={ev => ev.target.value ? this.addValue(ev.target.value) : null} >
          {[ '', ...this.props.options].map(option => <option key={option}>{option}</option>)}
        </select>
        <div className="selected" >{this.state.selectedValues.length > 0 ? this.state.selectedValues.map(value => <button key={value} onClick={ev => { ev.preventDefault(); this.removeValue(value)}}><Logo />{value}</button>):'no ' + this.props.name.toLowerCase() + ' selected'}
        </div>
      </label>
    );
  }
}

export default Filter;