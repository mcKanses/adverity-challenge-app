import React from 'react';
import Filter from './Filter';
import _ from 'lodash';

const getDistinctKeys = (data, columnName) => _.uniq(data.map(column => column[columnName]), columnName);

let filters = [];

const FilterSettings = (props) => {
    const { data } = props || [];
    
    const dimensions = [
      { name: 'Datasource'},
      { name: 'Campaign' }
    ];

    let filteredData;

    const filterData = data => {
      let filtered = data.slice(0);
      for (let [ key, values ] of Object.entries(filters)) {
        filtered = filtered.filter(data => values.includes(data[key]));
      }
      return filtered;
    }

    const handleSelectedValues = (key, values) => {
      filters[key] = values;
      if (!values || values.length < 1) delete filters[key];
    }

    const handleApply = ev => {
      if (ev?.preventDefault) ev.preventDefault();
      props.applyFilterHandler(filterData(data), filters);
    }

    return (
      <div id="filters">
        <h1>Filter dimension values</h1>
        {!data ? 'loading data...' :
          <form>
            {dimensions.map(
              dimension =>
              <Filter key={dimension.name} name={dimension.name} options={getDistinctKeys(data, dimension.name)} handleSelectedValues={values => handleSelectedValues(dimension.name, values)} />
            )}
            <button onClick={handleApply}>Apply</button>
          </form>
        }
      </div>
    )
}

export default FilterSettings;