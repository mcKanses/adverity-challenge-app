import React from 'react';

const Header = () =>
<header>
  <ul>
    <li>Select zero to N <span>Datasources</span></li>
    <li>Select zero to N <span>Campaigns</span></li>
  </ul>
  <span className="footnote">(where zero means "All")</span>
  <p>Hitting "Apply", filters the chart to show a timeseries for both Clicks and Impressions for given Datasources and Campaigns - logical AND</p>
 </header>

export default Header;