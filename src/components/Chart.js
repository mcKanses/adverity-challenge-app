import React, { Component } from 'react';
import { default as CanvasJSReact } from '../lib/canvasjs.react.js';
import _ from 'lodash';

// let CanvasJS = CanvasJSReact.CanvasJS;
let CanvasJSChart = CanvasJSReact.CanvasJSChart;



class Chart extends Component {
  constructor() {
		super();
    this.toggleDataSeries = this.toggleDataSeries.bind(this);
	}
	
	toggleDataSeries(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		this.chart.render();
	}
  render() {
    let { data, filteredData, filters } = this.props;
    if (!filters) filteredData = data;
    // if (filteredData) console.log(_.union(filteredData.map(data => data.Datasource)));
    // console.log(this.props?.data === this.props?.filteredData);
    
    const options = {
      theme: "light2",
      animationEnabled: true,
      title:{
        text:
          `Datasources: ${(this.props?.filters?.Datasource?.length > 0 ? this.props?.filters?.Datasource.map(ds => `"${ds}"`).join(', ') : 'All')}; Campaigns: ${(this.props?.filters?.Campaign?.length > 0 ? this.props?.filters?.Campaign.map(c => `"${c}"`).join(', ') : 'All')}`,
          fontSize: 18
      },
      subtitles: [{
        text: "Click Legend to Hide or Unhide Data Series"
      }],
      axisY: {
        title: "Clicks",
        titleFontColor: "#6D78AD",
        lineColor: "#6D78AD",
        labelFontColor: "#6D78AD",
        tickColor: "#6D78AD",
        includeZero: true
      },
      axisY2: {
        title: "Impressions",
        titleFontColor: "#51CDA0",
        lineColor: "#51CDA0",
        labelFontColor: "#51CDA0",
        tickColor: "#51CDA0",
        includeZero: true
      },
      toolTip: {
        shared: true
      },
      legend: {
        cursor: "pointer",
        itemclick: this.toggleDataSeries
      },
      data: [{
        type: "spline",
        name: "Clicks",
        showInLegend: true,
        xValueFormatString: "MMM YYYY",
        yValueFormatString: "#",
        // dataPoints: [
        //   { x: new Date(2017, 0, 1), y: 120 },
        //   { x: new Date(2017, 1, 1), y: 135 },
        //   { x: new Date(2017, 2, 1), y: 144 },
        //   { x: new Date(2017, 3, 1), y: 103 },
        //   { x: new Date(2017, 4, 1), y: 93 },
        //   { x: new Date(2017, 5, 1), y: 129 },
        //   { x: new Date(2017, 6, 1), y: 143 },
        //   { x: new Date(2017, 7, 1), y: 156 },
        //   { x: new Date(2017, 8, 1), y: 122 },
        //   { x: new Date(2017, 9, 1), y: 106 },
        //   { x: new Date(2017, 10, 1), y: 137 },
        //   { x: new Date(2017, 11, 1), y: 142 }
        // ],
        dataPoints: filteredData?.map(data => ({
          x: new Date(data.Date.split(".").reverse().join("-")), y: parseInt(data.Clicks)
        }))
      },
      {
        type: "spline",
        name: "Impressions",
        axisYType: "secondary",
        showInLegend: true,
        xValueFormatString: "MMM YYYY",
        yValueFormatString: "#",
        // dataPoints: [
        //   { x: new Date(2017, 0, 1), y: 19034.5 },
        //   { x: new Date(2017, 1, 1), y: 20015 },
        //   { x: new Date(2017, 2, 1), y: 27342 },
        //   { x: new Date(2017, 3, 1), y: 20088 },
        //   { x: new Date(2017, 4, 1), y: 20234 },
        //   { x: new Date(2017, 5, 1), y: 29034 },
        //   { x: new Date(2017, 6, 1), y: 30487 },
        //   { x: new Date(2017, 7, 1), y: 32523 },
        //   { x: new Date(2017, 8, 1), y: 20234 },
        //   { x: new Date(2017, 9, 1), y: 27234 },
        //   { x: new Date(2017, 10, 1), y: 33548 },
        //   { x: new Date(2017, 11, 1), y: 32534 }
        // ],
        dataPoints: filteredData?.map(data => ({
          x: new Date(data.Date.split(".").reverse().join("-")), y: parseInt(data.Impressions) || 0
        }))
      }]
    }
    return (
    <div>
      <CanvasJSChart options = {options} onRef={ref => this.chart = ref} />
    {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
  </div>);
  }
}

export default Chart;