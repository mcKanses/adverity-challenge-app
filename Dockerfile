FROM node:11

RUN apt-get update -y \
    && apt-get -y install curl build-essential git ca-certificates

RUN cd /usr/bin \
    && npx create-react-app adverity-challenge-app --use-npm

WORKDIR /usr/bin/adverity-challenge-app

COPY . .

RUN npm install

CMD [ "npm" , "start" ]